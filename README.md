CSE 6230, Fall 2014: Lab 3 -- CUDA
==================================

> Due: Sep 23, 2014 @ 4:35pm (just before class)

For instructions, see: https://bitbucket.org/gtcse6230fa14/lab3/wiki/Home


Implemented parallel list ranking for both Cilk and CUDA.

Initial approach was same as the pseudo-code given in the lecture slides for parallel list processing: Required creation of two auxiliary arrays and two parallel regions/kernels/_Cilk_fors for scanning and consolidating. Cilk gave a bandwidth of 0.125 Gbps with this implementation. (listrank-cilk_bkp.cc)
CUDA gave a bandwidth of 0.278 Gbps. (listrank-cuda_bkp.cu)

Then it was observed that the swapping of the next array could be done in place by alternating between the main and auxiliary array. This led to the bandwidth for CUDA rising to 0.291 Gbps. (listrank-cuda_bkp1.cu)

It was later found that if done smartly, even the Rank array could be updated inplace by alternating between the main and auxiliary array. With this update the CUDA performance rose to 0.308 Gbps. (listrank-cuda.cu). These changes were backported to the cilk code that rose the cilk performance to 0.163 Gbps (listrank-cilk.cc)

Final performance: CILK_BW = 0.163 Gbps, CUDA_BW = 0.308 Gbps

\m/ (-_-) \m/
