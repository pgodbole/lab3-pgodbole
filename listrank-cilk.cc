// -*- mode:c++; tab-width:2; indent-tabs-mode:nil;  -*-
/**
 *  \file listrank-cilk.cc
 *
 *  \brief Implement the 'listrank-par.hh' interface using Cilk Plus.
 */

#include <cassert>
#include <cstring>

#include <algorithm>
#include <iostream>

#include "listrank-par.hh"
#include <cilk/cilk.h>
#include <cilk/reducer_opadd.h>
#include <math.h>

using namespace std;

// ============================================================
const char *
getImplName__par (void)
{
  return "CILK";
}

// ============================================================

struct ParRankedList_t__
{
  size_t n;
  const index_t* Next;
  rank_t* Rank;
};

// ============================================================

ParRankedList_t *
setupRanks__par (size_t n, const index_t* Next)
{
  ParRankedList_t* L = new ParRankedList_t;
  assert (L);

  L->n = n;
  L->Next = Next;
  L->Rank = createRanksBuffer (n);

  return L;
}

void releaseRanks__par (ParRankedList_t* L)
{
  if (L) {
    releaseRanksBuffer (L->Rank);
  }
}

// ============================================================

const rank_t *
getRanks__par (const ParRankedList_t* L)
{
  return L->Rank;
}

// ============================================================

int intpow(int a, int b)
{
    return static_cast<int> (pow(a,b));
}

static void
computeListRanks__cilk__ (size_t n, const index_t* Next, rank_t* Rank)
{
    if (n == 0) return; // empty pool
    assert (Next);
    assert (Rank);
    // Initial values on which we will perform the list-based 'scan' /
    // 'prefix sum'
    index_t* next = new index_t[n]; assert (next);
    index_t* next_next = new index_t[n]; assert (next_next);
    rank_t* rank_next = new rank_t[n]; assert (rank_next);
    _Cilk_for (size_t i = 0; i < n; ++i)
    {
        next[i] = Next[i];
        if(Next[i]==NIL)
        {
            Rank[i] = 0;
            rank_next[i] = 0;
        }
        else
        {
            Rank[i] = 1;
            rank_next[i] = 1;
        }
    }
    int i = 0;
    while(i< log2(static_cast<double>(n)))
    {
        _Cilk_for(size_t j=0; j<n; ++j)
        {
            if(next[j]!=NIL)
            {
                rank_next[j] = Rank[j] + Rank[next[j]];
                next_next[j] = next[next[j]];
            }
            else
            {
                next_next[j] = NIL;
                rank_next[j] = Rank[j];
            }
        }
        ++i;
        if(i==log2(static_cast<double>(n))) break;
        _Cilk_for(size_t j=0; j<n; ++j)
        {
            if(next_next[j]!=NIL)
            {
                Rank[j] = rank_next[j] + rank_next[next_next[j]];
                next[j] = next_next[next_next[j]];
            }
            else
            {
                next[j] = NIL;
                Rank[j] = rank_next[j];
            }
        }
        ++i;
    }
    if(i%2==1)
    {
        _Cilk_for(size_t j=0; j<n; ++j)
        {
            Rank[j] = rank_next[j];
        }
    }
    //------------------------------------------------------------
    //
    // ... YOUR CODE GOES HERE ...
    //
    // (you may also modify any of the preceding code if you wish)
    //
    //#include "soln--cilk.cc" // Instructor's solution: none for you!
    //------------------------------------------------------------
}

void
computeListRanks__par (ParRankedList_t* L)
{
  assert (L != NULL);
  computeListRanks__cilk__ (L->n, L->Next, L->Rank);
}

// eof
